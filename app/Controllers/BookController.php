<?php
namespace App\Controllers;

use App\Repositories\BookRepositorie;

use App\Controllers\BaseController;

/**
* 
*/
class BookController extends BaseController
{
	/**
	 * [$app description]
	 * @var [type]
	 */
	private $app;

	/**
	 * [__construct description]
	 * @param [type] $app [description]
	 */
	public function __construct($app)
	{
		$this->app = $app;
		$this->book = new BookRepositorie;
	}

	/**
	 * [index description]
	 * @return [type] [description]
	 */
	public function index()
	{
		return $this->app->view->render('Home');
	}

	/**
	 * [postIndex description]
	 * @return [type] [description]
	 */
	public function postIndex()
	{
		$messages = [];
		
		$result = false;
		$numeros = [];

		$book = new BookRepositorie;

		// Regras de validação do formulário enviado 
		$form = $this->app->form->rules([
			'quantidade' => 'required|integer|min:1',
			'numeros' 	 => 'required|min:1'
		]);

		# formulário enviado é válido?
		if($form->isValid())
		{
			$quantidade = $this->app->input->get('quantidade');
			$numeros	= array_values(explode(',', $this->app->input->get('numeros')));

			# filtro na array para retornar apenas os números digitados
			$numeros = array_filter($numeros, function ($value)
			{
				return preg_replace('/\D/', '', $value);
			});

			# verifica se foi informado numeros repetidos.
			# quantidade de números informados
			# é quantidade correta?
			if(count($numeros) != $quantidade)
			{
				$messages[] = 'Por favor, informe quantidade de números corretamente';
			}

			# verifica se foi informado números repetidos
			if(count(array_unique($numeros)) < count($numeros))
			{
			    $messages[] = 'Por favor não informe números repetidos';
			}

			# do stuff...
			if(!count( $messages ))
			{
				$book->registerNumbers( $numeros );
				$book->getNumbers();

				# tudo ok?
				$result = true;
			}
			

		}else{

			# fail validation
			$messages = $form->messages();
		}

		return $this->app->view->render('Home', [
				'messages' 	=> $messages, 
				'result' 	=> $result,
				'numeros'	=> $numeros,
				'memoryUtilized' => $book->memoryInUse()
		]);
	}
}