<?php

namespace App\Core;

class Application
{
	/**
	 * [__construct description]
	 */
	public function __construct()
	{
		# code...
	}

	/**
	 * register instance
	 * @param  [type] $callback [description]
	 * @return [type]           [description]
	 */
	public function register($entitys, $closure = false)
	{
		if(is_array($entitys)){
			foreach ($entitys as $entity => $closure) {
				$this->$entity = call_user_func($closure);
			}
		}else{
			$this->$entitys = call_user_func($closure);
		}
	}
}