<?php

$app = new \App\Core\Application;
/**
 * Register container of class
 */
$app->register([
	'request' => function () use($app){
		return App\Core\Http\Request::instance($app);
	},
	'input' => function () use($app){
		return App\Core\Http\Input::instance($app);
	},
	'form' => function () use($app){
		return App\Core\Form\Validation::instance($app);
	},
	'response' => function () use($app){
		return App\Core\Http\Response::instance($app);
	},
	'uri' => function () use($app){
		return App\Core\Http\Uri::instance($app);
	},
	'url' => function () use($app){
		return App\Core\Http\Url::instance($app);
	},
	'redirect' => function () use($app){
		return App\Core\Http\Redirect::instance($app);
	},
	'route' => function () use($app){
		return App\Core\Routing\Router::instance($app);
	},
	'view' => function () use($app){
		return App\Core\Http\View::instance($app);
	},
]);

/**
 * Define route file
 */
include '../app/Routes.php';

$controller = App\Core\Routing\Controller::instance($app);

$controller->init();