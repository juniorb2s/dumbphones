<?php
namespace App\Core\Form;

/**
* 
*/
class Rules
{
	/**
	 * [FunctionName description]
	 * @param string $value [description]
	 */
	public function validateRequired($input)
	{
		if(isset($input) and empty($input)){
			return 'Campos obrigatórios não preenchidos';
		}

		return true;
	}

	/**
     * [FunctionName description]
     * @param string $value [description]
     */
    public function validateString($input)
    {
            if (!preg_match('/^[A-Za-z ]*$/', $input)) {
                return 'Permitido apenas letras A-Zaz and space';
            }
            return true;
    }

	/**
	 * [validateMin description]
	 * @param  [type] $input [description]
	 * @param  [type] $param [description]
	 * @return [type]        [description]
	 */
	public function validateMin($input, $param)
	{
		if(strlen($input) < $param){
			return 'Informe pelo menos '.$param . ' characteres';
		}
		
		return true;
	}

	/**
	 * [validateInteger description]
	 * @param  [type] $input [description]
	 * @return [type]        [description]
	 */
	public function validateInteger($input)
	{
		if (!preg_match('/^[0-9]*$/', $input)) {
		    return 'Permitido apenas letras números';
		}
		return true;
	}
}