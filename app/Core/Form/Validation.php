<?php
namespace App\Core\Form;
use App\Core\Singleton;
use App\Core\Form\Rules;
/**
* 
*/
class Validation extends Singleton
{
	/**
	 * [$_rules description]
	 * @var [type]
	 */
	private $_rules;

	/**
	 * [$_errors description]
	 * @var [type]
	 */
	private $_errors;

	/**
	 * [$_isValidForm description]
	 * @var [type]
	 */
	private $_isValidForm = true;

	/**
	 * [$_messages description]
	 * @var array
	 */
	private $_messages = [];

	/**
	 * [__construct description]
	 * @param [type] $app [description]
	 */
	function __construct($app)
	{
		$this->app = $app;
		$this->rules = new Rules;
	}

	/**
	 * [FunctionName description]
	 * @param string $value [description]
	 */
	public function rules($rules)
	{
		if(is_array($rules)){
			foreach ($rules as $input => $rules) {
				foreach ($this->explodeRules($rules) as $rule) {
					$this->validate($input, $rule);
				}				
			}
		}
		return $this;
	}

	/**
     * Validate a given attribute against a rule.
     *
     * @param  string  $attribute
     * @param  string  $rule
     * @return void
     */
    protected function validate($input, $rule)
    {
    	if(strpos($rule, ':')){
        	list($rule, $parameter) = explode(':', $rule);
        }

        if(strpos($parameter, ',')){
        	$parameter = $this->parseParameters($parameter);
        }

        $value = $this->app->input->get($input);
        $rule = ucfirst($rule);

        $method = "validate{$rule}";

        $result = $this->rules->$method($value, $parameter);

        if ( $result !== true) {
            $this->_messages[] = $result;
        } 
    }

    /**
     * [parseParameters description]
     * @param  [type] $parameter [description]
     * @return [type]            [description]
     */
    public function parseParameters($parameter)
    {
    	if(strpos($parameter, ',')){
        	return explode(',', $parameter);
        }
    }

    /**
     * [explodeRules description]
     * @param  [type] $rules [description]
     * @return [type]        [description]
     */
    protected function explodeRules($rules)
    {
        $rules = (is_string($rules)) ? explode('|', $rules) : $rules;
        return $rules;
    }

    /**
     * [isValid description]
     * @return boolean [description]
     */
    public function isValid()
    {
    	return ($this->_messages ? false : true);
    }

    /**
     * [messages description]
     * @return [type] [description]
     */
    public function messages()
    {
    	return $this->_messages;
    }

    /**
     * [isValidatable description]
     * @param  [type]  $rule [description]
     * @return boolean       [description]
     */
    public function isValidatable($rule)
    {
    	return true;
    }
}