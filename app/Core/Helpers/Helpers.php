<?php

/**
 * [env description]
 * @param  [type] $env [description]
 * @return [type]      [description]
 */
if(!function_exists('env')){
	function env($env)
	{
		return $_ENV[$env] ? $_ENV[$env] : null;
	}
}

/**
 * Verify exist key in array
 * @param  array $array 
 * @param  string $key   String of key. Permitted numbers with dot. P.x: 1.2.3
 * @return boolean
 */
if(!function_exists('array_has')){
	function array_has($array, $key)
	{
		if (empty($array) || is_null($key)) return false;
		if (array_key_exists($key, $array)) return true;
		foreach (explode('.', $key) as $segment)
		{
			if ( ! is_array($array) || ! array_key_exists($segment, $array))
			{
				return false;
			}
			$array = $array[$segment];
		}
		return true;
	}
}

/**
 * Return numbers dotted
 * @param  String  $numbers  Numbers
 * @param  integer $position 
 * @return string
 */
if(!function_exists('dotNumber')){
	function dotNumber($numbers, $position = 1)
	{
		return wordwrap( $numbers , $position, '.', true);
	}
}

if(!function_exists('dump')){
	function dump($value)
	{
		$trace = debug_backtrace();
	    $rootPath = dirname(dirname(__FILE__));
	    $file = str_replace($rootPath, '', $trace[0]['file']);
	    $line = $trace[0]['line'];
	    $var = $trace[0]['args'][0];
	    $lineInfo = sprintf('<div><strong>%s</strong> (line <strong>%s</strong>)</div>', $file, $line);
	    $debugInfo = sprintf('<pre>%s</pre>', print_r($var, true));
	    print_r($lineInfo.$debugInfo);
	}
}