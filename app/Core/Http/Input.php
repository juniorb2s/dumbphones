<?php
namespace App\Core\Http;
use App\Core\Singleton;

/**
* 
*/
class Input extends Singleton
{
	/**
	 * [$_input description]
	 * @var [type]
	 */
	private $_input;

	/**
	 * [__construct description]
	 * @param [type] $app [description]
	 */
	public function __construct($app)
	{
		$this->app = $app;
	}
	
	/**
	 * [get description]
	 * @param  [type] $input   [description]
	 * @param  [type] $default [description]
	 * @return [type]          [description]
	 */
	public function get($input, $default = null)
	{
		return (isset($_REQUEST[$input]) ? $_REQUEST[$input] : $default);
	}
}