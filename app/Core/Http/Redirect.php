<?php
namespace App\Core\Http;
use App\Core\Singleton;

/**
* 
*/
class Redirect extends Singleton
{

	/**
	 * [__construct description]
	 * @param [type] $app [description]
	 */
	public function __construct($app)
	{
		$this->app = $app;
	}

	/**
	 * [redirectTo description]
	 * @param  [type] $path [description]
	 * @return [type]       [description]
	 */
	public function to($path)
	{
		return header("Location:" . $path);
	}

	/**
	 * [back description]
	 * @return [type] [description]
	 */
	public function back()
	{
		return header("Location: ". $this->app->request->previousUrl());
	}
}