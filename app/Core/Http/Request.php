<?php
namespace App\Core\Http;
use App\Core\Singleton;
/**
* 
*/
class Request extends Singleton
{
	/**
	 * [$type description]
	 * @var [type]
	 */
	private $type;

	/**
	 * [$request description]
	 * @var [type]
	 */
	private $headerRequest;

	/**
	 * [$_previous_url description]
	 * @var [type]
	 */
	private $_previous_url;

	/**
	 * [__construct description]
	 */
	public function __construct()
	{
		$this->type = $_SERVER['REQUEST_METHOD'];
		$this->headerRequest = $_SERVER;
		$this->_previous_url = $this->headerRequest['REQUEST_URI'];
	}

	/**
	 * [previousUlr description]
	 * @return [type] [description]
	 */
	public function previousUrl()
	{
		return ($this->_previous_url ? $this->_previous_url : '/');
	}

	/**
	 * Retorna informações tipo de request
	 * @return [type] [description]
	 */
	public function getRequestType()
	{
		return $this->type;
	}

	/**
	 * Retorna informações do header da requisição
	 * @return [type] [description]
	 */
	public function getHeader()
	{
		return $this->headerRequest;
	}

	/**
	 * @param  [type]  $type [description]
	 * @return boolean       [description]
	 */
	public function is($type)
	{
		return ($type == $this->type);
	}
}