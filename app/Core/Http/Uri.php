<?php

namespace App\Core\Http;
use App\Core\Singleton;

class Uri extends Singleton
{
	/**
	 * [$uri description]
	 * @var [type]
	 */
	private $uri;

	public function getRequestURI()
	{
		$this->uri = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
		return $this->uri;
	}
}