<?php
namespace App\Core\Http;
use App\Core\Singleton;
/**
* 
*/
class View extends Singleton
{
	public function __construct($app)
	{
		$this->app = $app;
	}

	public function render($view, $data = [])
	{
		ob_start();
		extract($data);
		require('../public/views/' . $view . '.php');
		print utf8_decode(utf8_encode(ob_get_clean()));
	}
}