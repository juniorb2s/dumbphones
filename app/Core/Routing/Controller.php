<?php

namespace App\Core\Routing;
use App\Core\Singleton;
use ReflectionClass;

class Controller extends Singleton
{
	/**
     * The router instance.
     *
     * @var \Core\Routing\Router
     */
    private $router;

    public function __construct($app)
    {
    	$this->app = $app;
    }

    public function init()
    {
        $callback = $this->app->route->getCurrentRouteCallback();
        $class = $callback['class'];
        $method = $callback['method'];
        
        $class = new ReflectionClass( "\App\Controllers\\$class" );
        $instance = $class->newInstance($this->app);

        return $instance->$method();
    }

}