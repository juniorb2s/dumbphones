<?php

namespace App\Core\Routing;
use App\Core\Singleton;
use Exception;

class Router extends Singleton
{
	/**
	 * Routes
	 * @var array
	 */
	private $routes = [];

	/**
	 * [__construct description]
	 * @param [type] $app [description]
	 */
	public function __construct($app)
	{
		$this->uri = $app->uri;
		$this->request = $app->request;
	}

	/**
	 * Registra nova rota na aplicação
	 * @param  [type] $route    [description]
	 * @param  [type] $callback [description]
	 * @return [type]           [description]
	 */
    public function register($type, $route, $callback)
    {
    	$this->routes[$type][$route] = [
    		'class' 	=> 	$this->extractClass($callback),
    		'method'	=>	$this->extractMethod($callback)
    	];
    }

    /**
     * [extractClass description]
     * @param  [type] $callback [description]
     * @return [type]           [description]
     */
    public function extractClass($callback)
    {
    	return $callback = explode('@', $callback)[0];
    }

    /**
     * Retorna class de callback para rota atual
     * @return [type] [description]
     */
    public function getCurrentRouteCallback()
    {
    	$currentRequest = $this->uri->getRequestURI();
    	$currentRequestType = $this->request->getRequestType();

    	return $this->getCallbackToCurrentRoute($currentRequestType, $currentRequest);
    }

    /**
     * Extrai callback da rota atual
     * @param  [type] $requestType [description]
     * @param  [type] $requestPath [description]
     * @return [type]              [description]
     */
    public function getCallbackToCurrentRoute($requestType, $requestPath)
    {
    	$this->currentRoute = $requestPath;
    	$this->currentRouteCallback = $this->routes[$requestType][$requestPath];

    	return (array_key_exists($requestType, $this->routes) ? 
    		   ( array_key_exists($requestPath, $this->routes[$requestType]) ?  $this->routes[$requestType][$requestPath] : $this->invalidRouteCall()) 
    		   : $this->invalidRouteCall() );
    }

    /**
     * Exception
     * @return [type] [description]
     */
    public function invalidRouteCall()
    {
    	throw new Exception("Rota inválida", 1);
    }

    /**
     * Extrai método da rota registrada
     * @param  [type] $callback [description]
     * @return [type]           [description]
     */
    public function extractMethod($callback)
    {
    	return $callback = explode('@', $callback)[1];
    }

}