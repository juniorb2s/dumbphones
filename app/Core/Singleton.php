<?php
namespace App\Core;
/**
 * Abstract Singleton class to provide zero-configuration singleton functionality.
 *
 * @version 1.0
 */
abstract class Singleton {

	/**
	 * [$app description]
	 * @var [type]
	 */
	public static $app;
	
	/**
	 * Contains an array of instances to be served on getInstance calls.
	 *
	 * @var array
	 * @since 1.0
	 */
	private static $instances = array();
	
	/**
	 * Returns an instance of the class that calls the function (child classes).
	 * 
	 * @return object An instance of the calling class, by reference.
	 * @since 1.0
	 */
	final public static function &instance($app) {	
		$class = get_called_class();
		if(!isset(self::$instances[$class])) self::$instances[$class] = new $class($app);
		return self::$instances[$class];		
	}
	
}
?>