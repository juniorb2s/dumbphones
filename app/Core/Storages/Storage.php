<?php
namespace App\Core\Storages;

/**
* 
*/
interface Storage 
{
	/**
	 * [save description]
	 * @param  [type] $name [description]
	 * @param  [type] $file [description]
	 * @return [type]       [description]
	 */
	public function save($name, $file);

	/**
	 * [open description]
	 * @param  [type] $name [description]
	 * @return [type]       [description]
	 */
	public function open($name);

	/**
	 * [delete description]
	 * @param  [type] $name [description]
	 * @return [type]       [description]
	 */
	public function delete($name);
}