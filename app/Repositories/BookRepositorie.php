<?php
namespace App\Repositories;

use App\Repositories\MemoryRepositorie;

/**
* 
*/
class BookRepositorie extends MemoryRepositorie
{	

	/**
	 * [$_numbers description]
	 * @var [type]
	 */
	protected $_numbers = [];

	/**
	 * Registra os números na memória
	 * @return [type] [description]
	 */
	public function registerNumbers(Array $numbers)
	{
		$this->_numbers = $numbers;

		return $this;
	}

	/**
	 * Retorna todos os números da memória no momento
	 * @return [type] [description]
	 */
	public function getNumbers()
	{
		return $this->_numbers;
	}

	/**
	 * Quantidade de memória utilizada
	 * @return [type] [description]
	 */
	public function memoryInUse()
	{
		$memory = [];
		$memoryUtilized = 0;

		# loop numbers
		if(count($this->_numbers))
		{
			foreach ($this->_numbers as $key => $number) 
			{
				$processedNumber = '';

				# separe numbers
				$numberSplitted = str_split($number);

				# loop splitted numbers
				foreach ($numberSplitted as $number) 
				{	
					# which number far?
					$processedNumber .= $number;

					# dot number
					$dottedNumber = dotNumber($processedNumber);
					$memory = $this->getMemory();

					# is new point in memory?
					if(!array_has($memory, $dottedNumber))
					{
						# create point in memory
						foreach (explode(',', $dottedNumber) as $number) 
						{
							$this->setMemory($number, $number);
						}

						# new point in memory, add to memory used
						$memoryUtilized++;
					}
				}
			}

			return $memoryUtilized;

		}
	}
}