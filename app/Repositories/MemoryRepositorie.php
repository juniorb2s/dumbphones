<?php
namespace App\Repositories;

/**
* 
*/
class MemoryRepositorie 
{
	/**
	 * Inicializa o ponto na memória
	 * @var array
	 */
	protected $_memory;

	/**
	 * [$_size description]
	 * @var [type]
	 */
	protected $_size;

	public function setMemory($key, $value)
	{
		$this->_memory[$key] =  $value;

		return $this;
	}

	/**
	 * [get description]
	 * @return [type] [description]
	 */
	public function getMemory()
	{
		return $this->_memory;
	}

	/**
	 * [size description]
	 * @return [type] [description]
	 */
	public function size()
	{
		return $this->_size;
	}
}