<?php

define('BASE_PATH', realpath(__DIR__ . '/..'));
define('PUBLIC_PATH', realpath(__DIR__));

/**
 * Load vendor composer
 */
require '../vendor/autoload.php';

/**
 * Inicialize ENV DOT
 */

#$dotenv = new \Dotenv\Dotenv(BASE_PATH);
#$dotenv->load();

/**
 * 
 */
define('BASE_URL', env('BASE_URL'));

/**
 * Load bootstrap
 */
require '../app/Core/Bootstrap.php';

