<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Bootstrap 3, from LayoutIt!</title>

    <meta name="description" content="Source code generated using layoutit.com">
    <meta name="author" content="LayoutIt!">

    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
    <link href="css/tree.css" rel="stylesheet">

  </head>
  <body>

    <div class="container-fluid">
		<div class="row">
			<div class="col-md-6">				
				<form class="form-horizontal" method="post">
					<fieldset>
						<?php if(isset($messages) and $messages): ?>
							<ul>
							<?php foreach($messages as $message): ?>
								<li>
									<?=$message;?>
								</li>
							<?php endforeach; ?>
							</ul>
						<?php endif; ?>
						<!-- Form Name -->
						<legend>Adicionar números</legend>

						<!-- Text input-->
						<div class="form-group">
						  <label class="col-md-4 control-label" for="textinput">Quantidade de números</label>  
						  <div class="col-md-4">
						  <input id="textinput" name="quantidade" type="text" placeholder="quantidade" class="form-control input-md">
						  <span class="help-block">Informe quantidade de números</span>  
						  </div>
						</div>

						<!-- Textarea -->
						<div class="form-group">
						  <label class="col-md-4 control-label" for="textarea">Número</label>
						  <div class="col-md-4">                     
						    <textarea class="form-control" id="textarea" name="numeros"></textarea>
						    <span class="help-block">Informe os números, utilize a virgula após cada número</span>  
						  </div>
						</div>

					</fieldset>

					<button type="submit" class="pull-right btn btn-sm btn-success">Cadastrar numeros</button>
				</form>

			</div>
			<?php if( isset($result) and $result ): ?>
				<div class="col-md-6">
					<p>
						<legend>Agenda</legend>

						<h4>Números informados: </h4>
						<ul>
							<?php foreach($numeros as $numero): ?>
							<li><?=$numero;?></li>
						<?php endforeach; ?>
						</ul>

						<h4>Memória utilizada: <b><?=$memoryUtilized;?></b> </h4>
					</p>
					<!--
					We will create a family tree using just CSS(3)
					The markup will be simple nested lists
					-->
					<!--<div class="tree">
						<ul>
							<li>
								<a href="#">1</a>
								<ul>
									<li>
										<a href="#">2</a>
										<ul>
											<li>
												<a href="#">Grand Child</a>
											</li>
										</ul>
									</li>
									<li>
										<a href="#">3</a>
										<ul>
											<li><a href="#">Grand Child</a></li>
											<li>
												<a href="#">Grand Child</a>
												<ul>
													<li>
														<a href="#">Great Grand Child</a>
													</li>
													<li>
														<a href="#">Great Grand Child</a>
													</li>
													<li>
														<a href="#">Great Grand Child</a>
													</li>
												</ul>
											</li>
											<li><a href="#">Grand Child</a></li>
										</ul>
									</li>
								</ul>
							</li>

						</ul>
					</div>-->
				</div>
			<?php endif; ?>
		</div>
	</div>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.0.0-beta1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
  </body>
</html>